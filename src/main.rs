use pest::Parser;
#[macro_use]
extern crate pest_derive;

#[derive(Parser)]
#[grammar = "net.pest"]
pub struct NetParser;

use clap::*;
use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fs;

use eval::Value;

#[derive(Debug)]
struct Transition {
    inp: Vec<usize>,
    out: Vec<usize>,
    rate: String,
}

#[derive(Debug)]
enum ModelNumber {
    Float(f64),
    Int(usize),
    Str(String),
}
impl ModelNumber {
    fn from_str(s: &str) -> ModelNumber {
        match s.parse::<usize>() {
            Ok(x) => ModelNumber::Int(x),
            Err(_) => match s.parse::<f64>() {
                Ok(x) => ModelNumber::Float(x),
                Err(_) => ModelNumber::Str(s.to_string()),
            },
        }
    }
}
impl std::fmt::Display for ModelNumber {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ModelNumber::Float(x) => write!(f, "({})", x),
            ModelNumber::Int(x) => write!(f, "{}", x),
            ModelNumber::Str(x) => write!(f, "{}", x),
        }
    }
}

use askama::Template;
use std::fmt::Formatter;
use std::io::Write; // bring trait in scope

#[derive(Ord, PartialOrd, Eq, PartialEq, Clone)]
struct SpeciesCount {
    idx: usize,
    count: usize,
}

#[derive(Clone)]
struct FilledRule {
    inp: Vec<SpeciesCount>,
    out: Vec<SpeciesCount>,
    rate: f64,
    idx: usize,
    dependent_rules: Vec<usize>,
    full_dependet_rules: Vec<FilledRule>,
}
impl FilledRule {
    fn remove_duplicates(mut self) -> FilledRule {
        self.inp.sort();
        self.inp.dedup();
        self.out.sort();
        self.out.dedup();
        FilledRule {
            inp: self.inp,
            out: self.out,
            rate: self.rate,
            idx: self.idx,
            dependent_rules: self.dependent_rules,
            full_dependet_rules: self.full_dependet_rules,
        }
    }
}

fn eval_model_number(
    m: &ModelNumber,
    //constants_sorted: &Vec<&String>,
    constants: &HashMap<String, ModelNumber>,
) -> ModelNumber {
    //println!("{:?}", m);
    match m {
        ModelNumber::Float(f) => ModelNumber::Float(*f),
        ModelNumber::Int(k) => ModelNumber::Int(*k),
        ModelNumber::Str(s) => {
            let mut expr = eval::Expr::new(s);
            for k in constants.keys() {
                let val = constants.get(k).unwrap();
                expr = match val {
                    ModelNumber::Float(f) => expr.value(k, *f),
                    ModelNumber::Int(i) => expr.value(k, *i),
                    ModelNumber::Str(_) => {
                        unreachable!()
                    }
                }
            }
            //println!("{:?} -> {:?}", expr, expr.exec());
            match expr.exec().unwrap() {
                Value::Number(y) => match y.is_u64() {
                    true => ModelNumber::Int(y.as_u64().unwrap() as usize),
                    false => ModelNumber::Float(y.as_f64().unwrap()),
                },
                _ => unreachable!(),
            }
        }
    }
}

#[derive(Template)]
#[template(path = "template.c", escape = "none", print = "none")]
struct FilledModel {
    species_names: Vec<String>,
    species_initial_amt: Vec<usize>,
    species_inital_amt_indexed: Vec<(usize, usize)>,
    rules: Vec<FilledRule>,
    observation: Vec<(String, Vec<ObservationElementIdx>)>,
    observations_flat: FlatObservations,
}
impl FilledModel {
    pub fn get_dependet_rules(&self, r: &FilledRule) -> Vec<FilledRule> {
        let mut result: Vec<_> = r
            .dependent_rules
            .iter()
            .map(|i| self.rules[*i].clone())
            .collect();

        result.sort_by(|a, b| a.inp.len().partial_cmp(&b.inp.len()).unwrap());
        result
    }
    pub fn new(m: &Model) -> FilledModel {
        let mut constants: Vec<_> = m.constants.keys().collect();
        constants.sort_by_key(|v| usize::MAX - v.len());
        /*let mut sorted_species: Vec<(usize, String)> = m
        .species_initial_amount
        .iter()
        .map(|(a, b)| (*a, b.clone()))
        .collect();
        sorted_species.sort();*/
        let species_range = 1..={ m.species_names.len() };

        let species_initial_amt: Vec<usize> = species_range
            .clone()
            .map(|n| m.species_initial_amount.get(&n).unwrap())
            .map(|k| match eval_model_number(k, &m.constants) {
                ModelNumber::Float(f) => f as usize,
                ModelNumber::Int(k) => k,
                ModelNumber::Str(_) => unreachable!(),
            })
            .collect();

        let name_2_new_idx: HashMap<_, _> = species_range
            .clone()
            .map(|n| m.species_names.get(&n).unwrap().clone())
            .enumerate()
            .map(|(a, b)| (b, a))
            .collect();
        let observations = m
            .observations
            .iter()
            .map(|(n, d)| {
                (
                    n.clone(),
                    d.iter()
                        .map(|v| ObservationElementIdx {
                            count: v.count,
                            idx: *name_2_new_idx.get(&v.name).unwrap(), //v.idx - 1,
                        })
                        .collect(),
                )
            })
            .collect();
        FilledModel {
            species_names: species_range
                .clone()
                .map(|n| m.species_names.get(&n).unwrap().clone())
                .collect(),
            species_inital_amt_indexed: species_initial_amt
                .iter()
                .enumerate()
                .filter(|(_a, b)| **b != 0)
                .map(|(a, b)| (a, *b))
                .collect(),
            species_initial_amt,
            rules: m
                .rules
                .iter()
                .enumerate()
                .map(|(num, (_old_idx, r))| FilledRule {
                    idx: num,
                    full_dependet_rules: vec![],
                    inp: r
                        .inp
                        .iter()
                        .map(|k| SpeciesCount {
                            idx: k - 1,
                            count: r.inp.iter().filter(|v| *v == k).count(),
                        })
                        .collect(),
                    out: r
                        .out
                        .iter()
                        .map(|k| SpeciesCount {
                            idx: k - 1,
                            count: r.out.iter().filter(|v| *v == k).count(),
                        })
                        .collect(),
                    rate: match eval_model_number(&ModelNumber::Str(r.rate.clone()), &m.constants) {
                        //m.constants.get(&r.rate).unwrap() {
                        ModelNumber::Float(k) => k,
                        ModelNumber::Int(y) => y as f64,
                        ModelNumber::Str(_) => unreachable!(),
                    },
                    dependent_rules: vec![],
                })
                .map(|r| r.remove_duplicates())
                .collect(),
            observations_flat: FlatObservations::new(&observations),
            observation: observations,
        }
    }
    fn find_dependencies(&mut self) {
        let mut species_2_reac = vec![BTreeSet::new(); self.species_names.len()];
        for (num, rule) in self.rules.iter().enumerate() {
            for spec in rule.inp.iter().chain(rule.out.iter()) {
                species_2_reac[spec.idx].insert(num);
            }
        }

        let mut reac_2_reac = vec![BTreeSet::new(); self.rules.len()];
        for (num, rule) in self.rules.iter().enumerate() {
            for spec in rule.inp.iter().chain(rule.out.iter()) {
                reac_2_reac[num].extend(species_2_reac[spec.idx].iter());
            }
        }

        for (r, depen) in self.rules.iter_mut().zip(reac_2_reac.into_iter()) {
            r.dependent_rules = depen.into_iter().collect();
        }

        let rules = self.rules.clone();
        self.rules = rules
            .into_iter()
            .map(|mut rul| {
                rul.full_dependet_rules = self.get_dependet_rules(&rul);
                rul
            })
            .collect();
    }
}

#[derive(Debug, Clone)]
struct ObservationElement {
    count: usize,
    name: String,
}

#[derive(Debug, Clone)]
struct ObservationElementIdx {
    count: usize,
    idx: usize,
}

#[derive(Debug)]
struct FlatObservations {
    names: Vec<String>,
    linear_idixes: Vec<usize>,
    start_end: Vec<(usize, usize)>,
}
impl FlatObservations {
    fn new(dat: &Vec<(String, Vec<ObservationElementIdx>)>) -> FlatObservations {
        let names: Vec<String> = dat.iter().map(|(a, _b)| a).cloned().collect();
        let dat: HashMap<String, Vec<ObservationElementIdx>> = dat.iter().cloned().collect();
        let linear_idixes = names
            .iter()
            .map(|n| {
                dat.get(n)
                    .unwrap()
                    .iter()
                    .map(|v| std::iter::repeat(v.idx).take(v.count))
            })
            .flatten()
            .flatten()
            .collect();
        let counts: Vec<usize> = names
            .iter()
            .map(|n| dat.get(n).unwrap().iter().map(|v| v.count).sum())
            .collect();
        let start_end = counts
            .iter()
            .copied()
            .zip(counts.iter().scan(0, |sum, add| {
                *sum += add;
                Some(*sum)
            }))
            .collect();
        FlatObservations {
            names,
            linear_idixes,
            start_end,
        }
    }
}

#[derive(Template)]
#[template(path = "template.txt")]
#[derive(Debug)]
struct Model {
    species_names: BTreeMap<usize, String>,
    species_initial_amount: HashMap<usize, ModelNumber>,
    constants: HashMap<String, ModelNumber>,
    rules: HashMap<usize, Transition>,
    observations: Vec<(String, Vec<ObservationElement>)>,
}
impl Model {
    fn new(text: String) -> Model {
        let text = "\n".to_string() + &*text;
        let parsed = NetParser::parse(Rule::file, &text)
            .expect("Parsing error")
            .next()
            .unwrap();

        let mut parsed_iter = parsed.into_inner();
        let parameters = parsed_iter.next().unwrap();
        let species = parsed_iter.next().unwrap();
        let reactions = parsed_iter.next().unwrap();
        let observation_groups = parsed_iter.next().unwrap();
        let species_names: BTreeMap<usize, String> = species
            .clone()
            .into_inner()
            .map(|species_line| {
                let mut l = species_line.clone().into_inner();
                let id = l.next().unwrap().as_str().parse::<usize>().unwrap();
                let name = l.next().unwrap().as_str().to_string();
                return (id, name);
            })
            .collect();
        let observations = observation_groups
            .into_inner()
            .map(|obs_line| {
                let mut l = obs_line.into_inner();
                let _num = l.next().unwrap().as_str().parse::<usize>().unwrap();
                let name = l.next().unwrap().as_str().to_string();
                let obs_ents = l.next().unwrap();
                //println!("name {}", name);
                (
                    name,
                    obs_ents
                        .into_inner()
                        .map(|k| {
                            let count;
                            let idx;
                            match k.as_rule() {
                                Rule::int => {
                                    count = 1;
                                    idx = k.as_str().parse::<usize>().unwrap();
                                }
                                Rule::obs_multi => {
                                    let mut k = k.into_inner();
                                    count = k.next().unwrap().as_str().parse::<usize>().unwrap();
                                    idx = k.next().unwrap().as_str().parse::<usize>().unwrap();
                                }
                                _ => unreachable!(),
                            }
                            ObservationElement {
                                count,
                                name: species_names.get(&idx).unwrap().clone(),
                            }
                        })
                        .collect(),
                )
            })
            .collect();
        assert!(parsed_iter.next().is_none());
        Model {
            species_names,
            species_initial_amount: species
                .into_inner()
                .map(|species_line| {
                    let mut l = species_line.clone().into_inner();
                    let id = l.next().unwrap().as_str().parse::<usize>().unwrap();
                    let _name = l.next().unwrap().as_str().to_string();
                    let amt = ModelNumber::from_str(l.next().unwrap().as_str());
                    return (id, amt);
                })
                .collect(),
            constants: parameters
                .into_inner()
                .map(|parameter_line| {
                    let mut l = parameter_line.clone().into_inner();
                    let _num = l.next().unwrap().as_str().parse::<usize>().unwrap();
                    let name = l.next().unwrap().as_str().to_string();
                    let val = ModelNumber::from_str(l.next().unwrap().as_str());
                    (name, val)
                })
                .collect(),
            rules: reactions
                .into_inner()
                .map(|reaction_line| {
                    let mut l = reaction_line.clone().into_inner();
                    let num = l.next().unwrap().as_str().parse::<usize>().unwrap();
                    let inp: Vec<usize> = l
                        .next()
                        .unwrap()
                        .into_inner()
                        .map(|k| k.as_str().parse::<usize>().unwrap())
                        .collect();
                    let out: Vec<usize> = l
                        .next()
                        .unwrap()
                        .into_inner()
                        .map(|k| k.as_str().parse::<usize>().unwrap())
                        .collect();
                    (
                        num,
                        Transition {
                            inp,
                            out,
                            rate: l.next().unwrap().as_str().to_string(),
                        },
                    )
                })
                .collect(),
            observations,
        }
    }
}

fn main() {
    let matches = App::new("SSA code generator")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .get_matches();

    let text = fs::read_to_string(matches.value_of("INPUT").unwrap()).unwrap();
    let m = Model::new(text);

    let mut mf = FilledModel::new(&m);
    mf.find_dependencies();
    let mut file = std::fs::File::create("generated.c").unwrap();
    file.write_all(mf.render().unwrap().as_bytes()).unwrap();
}
