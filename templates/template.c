#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include "assert.h"
#include <stdint.h>

// RANDOM NUMBERS
struct xorshift128_state {
    uint32_t a, b, c, d;
};

/* The state array must be initialized to not be all zero */
uint32_t xorshift128(struct xorshift128_state *state)
{
    /* Algorithm "xor128" from p. 5 of Marsaglia, "Xorshift RNGs" */
    uint32_t t = state->d;

    uint32_t const s = state->a;
    state->d = state->c;
    state->c = state->b;
    state->b = s;

    t ^= t << 11;
    t ^= t >> 8;
    return state->a = t ^ s ^ (s >> 19);
}

struct splitmix64_state {
    uint64_t s;
};

uint64_t splitmix64(struct splitmix64_state *state) {
    uint64_t result = (state->s += 0x9E3779B97f4A7C15);
    result = (result ^ (result >> 30)) * 0xBF58476D1CE4E5B9;
    result = (result ^ (result >> 27)) * 0x94D049BB133111EB;
    return result ^ (result >> 31);
}

// as an example; one could do this same thing for any of the other generators
struct xorshift128_state xorshift128_init(uint64_t seed) {
    struct splitmix64_state smstate = {seed};
    struct xorshift128_state result = {0};

    uint64_t tmp = splitmix64(&smstate);
    result.a = (uint32_t)tmp;
    result.b = (uint32_t)(tmp >> 32);

    tmp = splitmix64(&smstate);
    result.c = (uint32_t)tmp;
    result.d = (uint32_t)(tmp >> 32);

    return result;
}

typedef struct{
    unsigned int amounts[{{species_names.len()}}];
    double propensities[{{rules.len()}}];
    double propensity_sum;
    double time;
    struct xorshift128_state random_state;

} state_t;

{% for r in rules %}
void execute_rule_{{-loop.index0-}}(state_t * s);
{% endfor %}


void set_initial_amounts(state_t * s){
    for (int i = 0; i < {{species_initial_amt.len()}}; i++){
        s->amounts[i] =0;
    }

    {%for (idx,amt) in species_inital_amt_indexed -%}
    s->amounts[{{idx}}] = {{amt}};
    {% endfor -%}
    s->time =0.;
    s->propensity_sum = 0.;
    for (int i = 0; i < {{rules.len()}}; i++){
        s->propensities[i] =0;
    }
}


{% for r in rules %}
void execute_rule_{{-loop.index0-}}(state_t * s){
    //printf("fire {{loop.index0}}\n");
    // Change state
    {%- for k in r.inp %}
    s->amounts[{{k.idx}}] -= {{k.count}};
    {%- endfor -%}
    {%- for k in r.out %}
    s->amounts[{{k.idx}}] += {{k.count}};
    {%- endfor -%}

    // change dependent Rates
    {% for d in r.full_dependet_rules %}
        s->propensity_sum -= s->propensities[{{d.idx}}];
        {%- for k in d.inp -%}
            {%- if k.count > 1 %}
            if (s->amounts[{{k.idx}}] < {{k.count}}) {
            s->propensities[{{-d.idx-}}] =  0.;
            } else
            {%- endif %}
        {%- endfor -%}
        s->propensities[{{-d.idx-}}] = {{d.rate}}
        {%- for k in d.inp %}
            * s->amounts[{{k.idx}}]
        {%- endfor -%};
        s->propensity_sum += s->propensities[{{d.idx}}];
    {% endfor %}
}
{% endfor %}

typedef void (*exec_function_t)(state_t*);
exec_function_t execute_functions[{{rules.len()}}] = {
{%- for r in rules %}
 execute_rule_{{-loop.index0-}},
{%- endfor %}
};

void calc_all_propensities(state_t * s){
    s->propensity_sum = 0;
    for(int idx = 0; idx < {{rules.len()}}; idx++){
        s->propensities[idx] = 0;
    }
    {% for r in rules %}
    s->propensities[{{-loop.index0-}}] = {{r.rate}}
    {%- for k in r.inp %} * s->amounts[{{k.idx}}]
    {%- endfor -%};
    {%- for k in r.inp %} {% if k.count > 1 %}
    if (s->amounts[{{k.idx}}] < {{k.count}}) {
        s->propensities[{{-loop.index0-}}] =  0.;
    }
    {%- endif -%}
    {%- endfor -%}
    s->propensity_sum += s->propensities[{{-loop.index0-}}];
    {% endfor %}

    if(s->propensity_sum < 0){
        printf("Internal error! (negative prop sum)\n");
        exit(-3);
    }
}

void init_random(state_t *s){
    s->random_state = xorshift128_init(time(NULL));
    for(int i=0;i<100;i++){
        xorshift128(&s->random_state);
    }
}

void init_determ(state_t *s,uint64_t seed){
    s->random_state = xorshift128_init(seed);
    for(int i=0;i<100;i++){
        xorshift128(&s->random_state);
    }
}

double get_uniform_double(state_t *s){
    double res = ((double) xorshift128(&s->random_state) )/((double) UINT32_MAX);
    //printf("%f -> gen %f, %f\n",s-> time,s->propensity_sum,res);
    return res;
}
double get_exponential_double(state_t *s, double lambda){
    double result =  -1./lambda * log(1-get_uniform_double(s));
    return result;
}

int step_part_1(state_t* s){
    if (s->propensity_sum <= 0){
        calc_all_propensities(s);
    }
    int idx = 0;
    const double threshold = get_uniform_double(s) * s->propensity_sum;
    double accumulated = 0.;
    while (accumulated + s->propensities[idx] < threshold ){
        accumulated += s->propensities[idx];
        idx++;
        if (idx > {{rules.len()}}){
            return -1;
        }
    }
    s->time += get_exponential_double(s,s->propensity_sum);
    return idx;
}

void step_part_2(state_t *s,int idx_of_reaction){
    execute_functions[idx_of_reaction](s);
}

const char* obs_names = "
{%- for (n,_) in observation -%}
{{n}},
{%- endfor -%}";

const unsigned int observation_fields[{{observations_flat.linear_idixes.len()}}] = { {% for i in observations_flat.linear_idixes %}{{i}},{% endfor %} };
const unsigned int observation_start_end[{{observations_flat.start_end.len()}}][2] = { {% for (a,b) in observations_flat.start_end %}{ {{a}},{{b}} },{% endfor %} };
struct observation_row{
   unsigned int dat[{{observations_flat.names.len()}}];
};
struct observation_row calc_observations(const state_t *s){
    struct observation_row res;
    for (int idx = 0; idx < {{observations_flat.names.len()}}; idx++){
        res.dat[idx] = 0;
            for (int i = observation_start_end[idx][1] - observation_start_end[idx][0]; i < observation_start_end[idx][1]; i++){
                res.dat[idx] += s->amounts[observation_fields[i]];
            }
        }
    return res;
}

void write_state_line(FILE* fptr,const state_t *s, double time){
    struct observation_row res = calc_observations(s);
    fprintf(fptr, "%g,", time);
    for (int i = 0; i < {{observations_flat.names.len()}}; i++) {
        fprintf(fptr, "%i,", res.dat[i]);
    }
    fprintf(fptr, "\n");
}

void run_and_writ(double target_time,state_t* s,unsigned  int num_observations){
    double last_write_time = -1e100;
    int obs_counter = 0;
    unsigned  int stepcounter = 0;
    FILE * fptr = fopen("out.csv","w");
    if(fptr == NULL)
    {
        printf("FILE OPEN ERROR!");
        exit(1);
    }
    fprintf(fptr,"#time,%s\n",obs_names);
    printf("## Execution started\n");
    while (s->time < target_time){

        const int idx = step_part_1(s);
        if (last_write_time + target_time/num_observations <= s->time || idx < 0 || obs_counter == 0) {
            write_state_line(fptr,s,obs_counter*target_time/num_observations);
            printf("\r## %f / %f",s->time,target_time);
            last_write_time = s->time;
            obs_counter++;
        }
        if (idx >= 0){
            step_part_2(s,idx);
        } else{
            s->time += target_time/num_observations;
        }
        stepcounter++;
    }
    printf("\ncompleted\nTotal number of steps: %i\n",stepcounter);
    write_state_line(fptr,s,target_time);
    fclose(fptr);
}

int main (int argc, char *argv[]) {
    if ((argc != 3) && (argc != 4)){
        printf("Error: this programm expects 2 or 3 command line arguments\n<simulation_time> <number_of_outputs> or <simulation_time> <number_of_outputs> <random seed>");
        return -1;
    }
    errno = 0;
    char *p;
    const int number_of_output = strtol(argv[2], &p, 10);
    if (errno != 0 || *p != '\0' | number_of_output <= 0) {
        printf("Error: could not read number of steps correctly. Is it a positive int?");
        return -2;
    }

    const double endtime = strtod(argv[1], &p);
    if (errno != 0 || *p != '\0' | endtime <= 0) {
        printf("Error: could not read endtime correctly. Is it a positive double?");
        return -2;
    }

    state_t s;

    if (argc == 4){
        const long random_seed = strtol(argv[3], &p, 10);
        if (errno != 0 || *p != '\0' | random_seed <= 0) {
            printf("Error: could not read random seed correctly. Is it a positive int?");
            return -3;
        }
        init_determ(&s,random_seed);
        printf("Deterministic run (for this specific code) until %f\n", endtime);

    } else {
        printf("Random run (not very random) until %f\n", endtime);
        init_random(&s);
    }

    set_initial_amounts(&s);
    calc_all_propensities(&s);

    run_and_writ(endtime,&s,number_of_output);
    return 0;
}

