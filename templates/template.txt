{% for (n,s) in species_names %}{{n}} -> {{species_initial_amount.get(n).unwrap()}} {{s}}
{% endfor %}

{% for (_,r) in rules -%}
{% for i in r.inp -%}
{%if !loop.first %} + {%endif%} {{species_names.get(i).unwrap()}}
{%- endfor %} ->
{%- for i in r.out -%}
{%if !loop.first %} + {%endif%} {{species_names.get(i).unwrap()}}
{%- endfor %} @ ...
{% endfor %}